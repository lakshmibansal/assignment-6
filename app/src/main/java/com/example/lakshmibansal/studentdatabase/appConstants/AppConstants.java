package com.example.lakshmibansal.studentdatabase.appConstants;

public interface AppConstants {
    int EDIT_REQUEST = 200;
    int ADD_REQUEST = 100;
    String NULL_STRING = "";
    //colors
    String BLACK = "#000000";
    String WHITE = "#ffffff";
    String SELECTED_COLOR = "#ffff99";
    String UNSELECTED_COLOR = "#029456";

    // dropdown options
    String DEFAULT_OPTION = "Sort By";
    String SORT_BY_NAME = "Name";
    int SORT_NAME = 0;
    int SORT_ROLL = 1;
    String SORT_BY_ROLLNO = "RollNo";
    String[]  TABS = {"LIST","GRID"};
    String VIEW_BUTTON = "View";
    String EDIT_BUTTON = "Edit";

    String UPDATE_TEXT = "Update";
    String VIEW_ACTIVITY_TITLE = "View Student";
    String EDIT_ACTIVITY_TITLE = "Edit Student";
    String CLOSE_BUTTON_TEXT = "Close";

    //messages
    String CANCEL_MESSAGE = "Cancelled Operation!";

    //KEYS
    String BUTTON_KEY = "button";
    String STUDENT_KEY = "student";
    String POSITION_KEY = "position";

    String DIALOG_TITLE = "Options";

    String UPDATE_KEY = "Update";
    String INSERT_KEY = "Insert";
    String DELETE_KEY = "Delete";
    String VIEW_ALL_KEY = "View All";
    String VIEW_KEY = "View Single";
    int LIST = 0;
    int GRID = 1;
    int OPERATION_KEY = 1;
    int STUDENT_KEY_INDEX = 0;
    int INITIAL_VALUE = 1;
    int FINAL_VALUE = 101;
    long SLEEP_TIME = 10;
    int FAILED_OPERATION = -1;
    int ADDED_SUCCESS = 1;
    int UPDATE_SUCCESS = 2;
    int DELETE_SUCCESS = 3;
    int EXTRACTION_SUCCESS = 4;
    int VIEW_SUCCESS = 5;
    String DIALOG_MESSAGE = "Completed";
    String ALERT_TITLE = "Sign Up Successful";
    String ALERT_MESSAGE = "Now Please Login to continue.";
    String INAPPROPRIATE_ROLL_MESSAGE = "Roll Number cannot be left blank or can be ZERO!";
    String INAPPROPRIATE_NAME_MESSAGE = "Name cannot Start with Space or Left Blank!";
    String USERNAME_KEY = "username";
    String PASSWORD_DEFAULT = "*******";
    String USERNAME_DEFAULT = "INVALID";
    String PASSWORD_KEY = "password";
    String INVALID_USERNAME_MESSAGE = "Invalid username! Please enter CORRECT credentials.";
    String INVALID_PASSWORD_MESSAGE = "Invalid password! Please enter CORRECT credentials.";
    String INVALID_EMAIL_MESSAGE = "Invalid email! Please enter CORRECT email id.";
    String BLANK_FIELD_MESSAGE = "None of the field can be left blank.";
    String SIGN_UP = "Sign Up";
    String LOGIN = "Login";

    String FAILED_OPERATION_MESSAGE = "Failed to insert this entry! Try another. Make sure " +
            "duplicate roll number is not inserted.";
    String NO_ENTRY_FOUND_MESSAGE = "No Entry found in the database.";
    String ADDED_SUCCESS_MESSAGE = "Successfully Added Entry.";
    String UPDATE_SUCCESS_MESSAGE = "Successfully Updated Entry.";
    String DELETE_SUCCESS_MESSAGE = "Successfully Deleted Entry.";
}
