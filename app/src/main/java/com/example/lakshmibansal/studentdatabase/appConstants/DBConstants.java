package com.example.lakshmibansal.studentdatabase.appConstants;

/**
 * Created by LAKSHMI BANSAL on 09/02/2015.
 */
public interface DBConstants {
    String DB_NAME = "Student_DB";
    int DB_VERSION = 1;
    String TABLE_NAME = "student_info";
    String ROLL_COLUMN = "roll_no";
    String NAME_COLUMN = "name";
    String EMAIL_COLUMN = "email";
    String DEPARTMENT_COLUMN = "department";
    String ADDRESS_COLUMN = "address";
    int DATABASE_VERSION = 2;

}
