package com.example.lakshmibansal.studentdatabase.appConstants;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.lakshmibansal.studentdatabase.entities.Student;

import java.util.ArrayList;
import java.util.List;

import static com.example.lakshmibansal.studentdatabase.appConstants.DBConstants.*;

public class DBController implements AppConstants{
   Context context;
    DBHelper dbHelper;
    SQLiteDatabase database;

    public DBController(Context context){
        this.context = context;
    }

    public void open(){
        dbHelper = new DBHelper(context,DB_NAME,null,DB_VERSION);
        database = dbHelper.getWritableDatabase();
    }

    public void close(){
        database.close();
    }

    public long insert(Student student){
        ContentValues contentValues = new ContentValues();
        contentValues.put(ROLL_COLUMN,student.getRollno());
        contentValues.put(NAME_COLUMN,student.getName());
        contentValues.put(EMAIL_COLUMN,student.getEmail());
        contentValues.put(DEPARTMENT_COLUMN,student.getDepartment());
        contentValues.put(ADDRESS_COLUMN,student.getAddress());
        return database.insert(TABLE_NAME, null, contentValues);
    }

    //
    public int update(Student student){
        ContentValues contentValues = new ContentValues();
        contentValues.put(NAME_COLUMN,student.getName());
        contentValues.put(EMAIL_COLUMN,student.getEmail());
        contentValues.put(DEPARTMENT_COLUMN,student.getDepartment());
        contentValues.put(ADDRESS_COLUMN,student.getAddress());
        return database.update(TABLE_NAME,contentValues,ROLL_COLUMN + "=?",
                new String[]{Integer.toString(student.getRollno())});
    }

    //
    public int delete(Student student){
        return database.delete(TABLE_NAME,ROLL_COLUMN+"=?",
                new String[]{Integer.toString(student.getRollno())});
    }

    //extract all data from the database
    public List<Student> viewAllStudent(){
        int roll;
        String name, email, department, address;
        List<Student> tempList = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT * FROM "+TABLE_NAME,null);
        if(cursor.getCount()>0){
            cursor.moveToFirst();
            while(!cursor.isAfterLast()){
                roll = cursor.getInt(cursor.getColumnIndex(ROLL_COLUMN));
                name = cursor.getString(cursor.getColumnIndex(NAME_COLUMN));
                email = cursor.getString(cursor.getColumnIndex(EMAIL_COLUMN));
                department = cursor.getString(cursor.getColumnIndex(DEPARTMENT_COLUMN));
                address = cursor.getString(cursor.getColumnIndex(ADDRESS_COLUMN));
                tempList.add(new Student(name,roll,email,address,department));
                cursor.moveToNext();
            }
        }
        return tempList;
    }

    // This method will extract single row from database
    public Student viewSingleStudents(int rollNumber){
        Student tempStudent = null;
        int roll;
        String name, email, department, address;
        Cursor cursor = database.rawQuery("SELECT * FROM "+ TABLE_NAME +
                " WHERE " + ROLL_COLUMN + "= "+rollNumber, null);
        if(cursor.getCount()>0){
            cursor.moveToFirst();
            if(!cursor.isAfterLast()){
                roll = cursor.getInt(cursor.getColumnIndex(ROLL_COLUMN));
                name = cursor.getString(cursor.getColumnIndex(NAME_COLUMN));
                email = cursor.getString(cursor.getColumnIndex(EMAIL_COLUMN));
                department = cursor.getString(cursor.getColumnIndex(DEPARTMENT_COLUMN));
                address = cursor.getString(cursor.getColumnIndex(ADDRESS_COLUMN));
                tempStudent = new Student(name,roll,email,address,department);
            }
        }
        return tempStudent;
    }
}
