package com.example.lakshmibansal.studentdatabase.appConstants;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import static com.example.lakshmibansal.studentdatabase.appConstants.DBConstants.*;

public class DBHelper extends SQLiteOpenHelper {

    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_NAME + " (" +
                        ROLL_COLUMN + " INTEGER PRIMARY KEY , "+
                        NAME_COLUMN + " TEXT NOT NULL, "+
                        EMAIL_COLUMN + " TEXT NOT NULL, "+
                        DEPARTMENT_COLUMN + " TEXT NOT NULL, "+
                        ADDRESS_COLUMN + " TEXT NOT NULL);"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_NAME);
        onCreate(db);
    }
}
