
package com.example.lakshmibansal.studentdatabase.entities;

import java.io.Serializable;

public class Student implements Serializable {

    private String name;
    private int rollno;
    private String email;
    private String department;
    private String address;

    public Student(String name, int rollno,String email,String address,String department ) {
        this.name = name;
        this.rollno = rollno;
        this.address = address;
        this.department = department;
        this.email = email;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {

        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRollno() {
        return this.rollno;
    }

    public void setRollno(int rollno) {
        this.rollno = rollno;
    }
}
