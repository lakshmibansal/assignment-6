package com.example.lakshmibansal.studentdatabase.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.lakshmibansal.studentdatabase.R;
import com.example.lakshmibansal.studentdatabase.util.ListAdapter;

import static com.example.lakshmibansal.studentdatabase.appConstants.AppConstants.DELETE_KEY;
import static com.example.lakshmibansal.studentdatabase.appConstants.AppConstants.DIALOG_TITLE;
import static com.example.lakshmibansal.studentdatabase.appConstants.AppConstants.UPDATE_KEY;
import static com.example.lakshmibansal.studentdatabase.appConstants.AppConstants.VIEW_KEY;

public class ListFragmentView extends Fragment {
    ListView listview;
    int objectPosition;
    ListAdapter adapter;

    public ListFragmentView() {
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_fragment, container, false);
        listview = (ListView) view.findViewById(R.id.listView);

        SpinnerFragment spinnerFragment = new SpinnerFragment();
        getChildFragmentManager().beginTransaction().replace(R.id.container, spinnerFragment).commit();

        if (getActivity() instanceof SlideDrawer) {
            adapter = new ListAdapter(getActivity());
            if (!(this.getArguments() == null)) {
                listview.setAdapter((ListAdapter) this.getArguments().getSerializable("adapter"));
            }
        }
        // Setting Item Click Listeners for ListView
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                click(parent, view, position, id);
            }
        });
        return view;
    }

    public void click(AdapterView<?> parent, View view, final int position, long id) {
        final Dialog options = new Dialog(getActivity());
        options.setContentView(R.layout.item_dialog);
        options.setTitle(DIALOG_TITLE);
        Button viewBtn = (Button) options.findViewById(R.id.viewBtn);
        Button editBtn = (Button) options.findViewById(R.id.editBtn);
        Button deleteBtn = (Button) options.findViewById(R.id.deleteBtn);
        viewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() instanceof SlideDrawer)
                    ((SlideDrawer) getActivity()).new DBinteraction().execute(adapter.getItem(position), VIEW_KEY);
                options.dismiss();
            }
        });
        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                objectPosition = position;
                if (getActivity() instanceof SlideDrawer)
                    ((SlideDrawer) getActivity()).new DBinteraction().execute(adapter.getItem(position), UPDATE_KEY);
                options.dismiss();
            }
        });
        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() instanceof SlideDrawer)
                    ((SlideDrawer) getActivity()).new DBinteraction().execute(adapter.getItem(position), DELETE_KEY);
                adapter.student.remove(position);
                adapter.notifyDataSetChanged();
                options.dismiss();
            }
        });
        options.show();
    }
}
