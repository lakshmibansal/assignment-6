package com.example.lakshmibansal.studentdatabase.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.lakshmibansal.studentdatabase.R;
import com.example.lakshmibansal.studentdatabase.appConstants.DBController;
import com.example.lakshmibansal.studentdatabase.entities.Student;
import com.example.lakshmibansal.studentdatabase.util.ListAdapter;
import com.example.lakshmibansal.studentdatabase.util.LoginActivity;
import com.example.lakshmibansal.studentdatabase.util.StudentActivity;
import com.example.lakshmibansal.studentdatabase.util.pagerAdapter;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;

import static com.example.lakshmibansal.studentdatabase.appConstants.AppConstants.ADD_REQUEST;
import static com.example.lakshmibansal.studentdatabase.appConstants.AppConstants.BUTTON_KEY;
import static com.example.lakshmibansal.studentdatabase.appConstants.AppConstants.DELETE_KEY;
import static com.example.lakshmibansal.studentdatabase.appConstants.AppConstants.DELETE_SUCCESS;
import static com.example.lakshmibansal.studentdatabase.appConstants.AppConstants.DELETE_SUCCESS_MESSAGE;
import static com.example.lakshmibansal.studentdatabase.appConstants.AppConstants.EDIT_BUTTON;
import static com.example.lakshmibansal.studentdatabase.appConstants.AppConstants.EDIT_REQUEST;
import static com.example.lakshmibansal.studentdatabase.appConstants.AppConstants.EXTRACTION_SUCCESS;
import static com.example.lakshmibansal.studentdatabase.appConstants.AppConstants.FAILED_OPERATION;
import static com.example.lakshmibansal.studentdatabase.appConstants.AppConstants.GRID;
import static com.example.lakshmibansal.studentdatabase.appConstants.AppConstants.LIST;
import static com.example.lakshmibansal.studentdatabase.appConstants.AppConstants.NO_ENTRY_FOUND_MESSAGE;
import static com.example.lakshmibansal.studentdatabase.appConstants.AppConstants.OPERATION_KEY;
import static com.example.lakshmibansal.studentdatabase.appConstants.AppConstants.POSITION_KEY;
import static com.example.lakshmibansal.studentdatabase.appConstants.AppConstants.SORT_NAME;
import static com.example.lakshmibansal.studentdatabase.appConstants.AppConstants.SORT_ROLL;
import static com.example.lakshmibansal.studentdatabase.appConstants.AppConstants.STUDENT_KEY;
import static com.example.lakshmibansal.studentdatabase.appConstants.AppConstants.STUDENT_KEY_INDEX;
import static com.example.lakshmibansal.studentdatabase.appConstants.AppConstants.TABS;
import static com.example.lakshmibansal.studentdatabase.appConstants.AppConstants.UPDATE_KEY;
import static com.example.lakshmibansal.studentdatabase.appConstants.AppConstants.UPDATE_SUCCESS;
import static com.example.lakshmibansal.studentdatabase.appConstants.AppConstants.VIEW_ALL_KEY;
import static com.example.lakshmibansal.studentdatabase.appConstants.AppConstants.VIEW_BUTTON;
import static com.example.lakshmibansal.studentdatabase.appConstants.AppConstants.VIEW_KEY;
import static com.example.lakshmibansal.studentdatabase.appConstants.AppConstants.VIEW_SUCCESS;

public class SlideDrawer extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks, SpinnerFragment.OnDataPass, Serializable {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    transient private CharSequence mTitle;
    transient ViewPager pager;
    transient int objectPosition;
    transient Student studentObject;
    ListAdapter adapter;
    transient ActionBar actionBar;
    transient ActionBar.Tab tab;
    transient Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slide_drawer);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();
        adapter = new ListAdapter(SlideDrawer.this);
        initComponents();
        new DBinteraction().execute(null, VIEW_ALL_KEY);

    }

    public void initComponents() {
        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
        bundle = new Bundle();
        bundle.putSerializable("adapter", adapter);
        pager = (ViewPager) findViewById(R.id.viewPager);
        pager.setAdapter(new pagerAdapter(getSupportFragmentManager(), bundle));
        actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        actionBar.show();

        // Create a tab listener that is called when the user changes tabs.
        ActionBar.TabListener tabListener = new ActionBar.TabListener() {
            public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
                int position = tab.getPosition();
                pager.setCurrentItem(position);
            }

            public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
            }

            public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
            }
        };

        for (int i = 0; i < TABS.length; i++) {
            tab = actionBar.newTab()
                    .setText(TABS[i])
                    .setTabListener(tabListener);

            actionBar.addTab(tab);
        }

        pager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                actionBar.setSelectedNavigationItem(position);
            }
        });

    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, PlaceholderFragment.newInstance(position + 1))
                .commit();
    }

    public void onSectionAttached(int number) {
        Intent intent;
        switch (number) {
            case 1:
                mTitle = "HOME";
                break;
            case 2:
                mTitle = "Add Student";
                intent = new Intent(this, StudentActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivityForResult(intent, ADD_REQUEST);
                break;
            case 3:
                mTitle = "Logout";
                intent = new Intent(SlideDrawer.this, LoginActivity.class);
                startActivity(intent);
                break;

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ADD_REQUEST) {
            if (resultCode == RESULT_OK) {
                bundle = data.getExtras();
                adapter.student.add((Student) bundle.get(STUDENT_KEY));
                adapter.notifyDataSetChanged();
                onSectionAttached(1);
            } else {
                onSectionAttached(1);
            }
        } else if (requestCode == EDIT_REQUEST) {
            if (resultCode == RESULT_OK) {
                bundle = data.getExtras();
                adapter.student.set(bundle.getInt(POSITION_KEY), (Student) bundle.get(STUDENT_KEY));
                adapter.notifyDataSetChanged();
                onSectionAttached(1);
            } else {
                onSectionAttached(1);
            }
        }
    }

    public void restoreActionBar() {
        actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.slide_drawer, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDataPasser(int value) {
        if (value == SORT_NAME) {
            sortByName();
        } else if (value == SORT_ROLL) {
            sortByRoll();
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_slide_drawer, container, false);
            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((SlideDrawer) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
        }

    }

    public void sortByName() {
        Collections.sort(adapter.student, new Comparator<Student>() {
            @Override
            public int compare(Student lhs, Student rhs) {
                return lhs.getName().compareTo(rhs.getName());
            }
        });
        adapter.notifyDataSetChanged();
    }

    public void sortByRoll() {
        Collections.sort(adapter.student, new Comparator<Student>() {
            @Override
            public int compare(Student lhs, Student rhs) {
                return lhs.getRollno() - (rhs.getRollno());
            }
        });
        adapter.notifyDataSetChanged();
    }

    class DBinteraction extends AsyncTask<Object, Integer, Integer> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(Object... params) {
            DBController dbController = new DBController(SlideDrawer.this);
            // For Extracting all values from database
            if (params[OPERATION_KEY].equals(VIEW_ALL_KEY)) {
                dbController.open();
                adapter.student = dbController.viewAllStudent();
                dbController.close();
                if (adapter.student.size() > 0) {
                    adapter.notifyDataSetChanged();
                    return EXTRACTION_SUCCESS;
                }

            } else if (params[OPERATION_KEY].equals(DELETE_KEY)) {
                dbController.open();
                int result = dbController.delete((Student) params[STUDENT_KEY_INDEX]);
                dbController.close();
                if (result > 0) {
                    return DELETE_SUCCESS;
                }

            } else if (params[OPERATION_KEY].equals(UPDATE_KEY)) {
                studentObject = (Student) params[STUDENT_KEY_INDEX];
                int rollNumber = studentObject.getRollno();
                dbController.open();
                studentObject = dbController.viewSingleStudents(rollNumber);
                dbController.close();
                if (studentObject.getRollno() > 0) {
                    return UPDATE_SUCCESS;
                }

            } else if (params[OPERATION_KEY].equals(VIEW_KEY)) {
                dbController.open();
                studentObject = (Student) params[STUDENT_KEY_INDEX];
                int rollNumber = studentObject.getRollno();
                studentObject = dbController.viewSingleStudents(rollNumber);
                dbController.close();
                if (studentObject.getRollno() > 0) {
                    return VIEW_SUCCESS;
                }
            }

            return FAILED_OPERATION;
        }

        @Override
        protected void onPostExecute(Integer integer) {

            // If the operation performed is failed
            if (integer == FAILED_OPERATION) {
                Toast.makeText(SlideDrawer.this,
                        NO_ENTRY_FOUND_MESSAGE, Toast.LENGTH_SHORT).show();
            }

            // For Extraction is successful
            else if (integer == EXTRACTION_SUCCESS) {
                sortByRoll();
            }

            // For Deletion is successful
            else if (integer == DELETE_SUCCESS) {

                Toast.makeText(SlideDrawer.this,
                        DELETE_SUCCESS_MESSAGE, Toast.LENGTH_SHORT).show();
            }
            // For single Extraction is successful and open view activity(StudentOperations)
            else if (integer == VIEW_SUCCESS) {
                Intent intent = new Intent(SlideDrawer.this, StudentActivity.class);
                intent.putExtra(BUTTON_KEY, VIEW_BUTTON);
                intent.putExtra(STUDENT_KEY, studentObject);
                startActivity(intent);
            }
            // For single Extraction is successful and open edit activity(StudentOperations)
            else if (integer == UPDATE_SUCCESS) {
                Intent intent = new Intent(SlideDrawer.this, StudentActivity.class);
                intent.putExtra(BUTTON_KEY, EDIT_BUTTON);
                intent.putExtra(POSITION_KEY, objectPosition);
                intent.putExtra(STUDENT_KEY, studentObject);
                startActivityForResult(intent, EDIT_REQUEST);
            }
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }
    }

}
