package com.example.lakshmibansal.studentdatabase.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.lakshmibansal.studentdatabase.R;

import static com.example.lakshmibansal.studentdatabase.appConstants.AppConstants.DEFAULT_OPTION;
import static com.example.lakshmibansal.studentdatabase.appConstants.AppConstants.SORT_BY_NAME;
import static com.example.lakshmibansal.studentdatabase.appConstants.AppConstants.SORT_BY_ROLLNO;
import static com.example.lakshmibansal.studentdatabase.appConstants.AppConstants.SORT_NAME;
import static com.example.lakshmibansal.studentdatabase.appConstants.AppConstants.SORT_ROLL;

/**
 * Created by LAKSHMI BANSAL on 21/02/2015.
 */

public class SpinnerFragment extends Fragment {
    Spinner dropdown;

    int sortOption = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_spinner, container, false);
        dropdown = (Spinner) view.findViewById(R.id.sort_By);
        final String[] sortItems = {DEFAULT_OPTION, SORT_BY_NAME, SORT_BY_ROLLNO};
        ArrayAdapter<String> spinner_adapter = new ArrayAdapter<String>(getActivity().getApplicationContext()
                , android.R.layout.simple_spinner_dropdown_item, sortItems) {
            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View v = null;
                if (position == 0) {
                    TextView tv = new TextView(getContext());
                    tv.setHeight(0);
                    v = tv;
                } else {
                    v = super.getDropDownView(position, null, parent);
                }
                return v;
            }
        };

        dropdown.setAdapter(spinner_adapter);
        dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String choosedItem = (String) parent.getItemAtPosition(position);
                if (choosedItem.equals(SORT_BY_NAME)) {
                    sortOption = SORT_NAME;
                    ((OnDataPass) getActivity()).onDataPasser(sortOption);

                } else if (choosedItem.equals(SORT_BY_ROLLNO)) {
                    sortOption = SORT_ROLL;
                    ((OnDataPass) getActivity()).onDataPasser(sortOption);
                }
                dropdown.setSelection(0);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        return view;
    }

    public interface OnDataPass {
        public void onDataPasser(int value);
    }
}

