package com.example.lakshmibansal.studentdatabase.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.lakshmibansal.studentdatabase.R;
import com.example.lakshmibansal.studentdatabase.fragments.SlideDrawer;

import static com.example.lakshmibansal.studentdatabase.appConstants.AppConstants.*;

public class LoginActivity extends Activity {
    EditText user, pass;
    Button login;
    String userName, password;
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        user = (EditText) findViewById(R.id.user_box);
        pass = (EditText) findViewById(R.id.pass_box);
        login = (Button) findViewById(R.id.loginBtn);

        preferences = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);
        if (preferences.getString(USERNAME_KEY, USERNAME_DEFAULT).equals(USERNAME_DEFAULT)) {
            login.setText(SIGN_UP);
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.loginBtn:
                checkUser();
                break;
            case R.id.cancelBtn:
        //        finish();
                Intent intent = new Intent(this, SlideDrawer.class);
                startActivity(intent);
                break;
        }
    }

    public void checkUser() {
        userName = user.getText().toString();
        password = pass.getText().toString();
        if (userName.equals("") || pass.equals("")) {
            Toast.makeText(LoginActivity.this, BLANK_FIELD_MESSAGE, Toast.LENGTH_SHORT).show();
        } else {
            if (preferences.getString(USERNAME_KEY, USERNAME_DEFAULT).equals(userName)) {
                if (preferences.getString(PASSWORD_KEY, PASSWORD_DEFAULT).equals(password)) {
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(LoginActivity.this, INVALID_PASSWORD_MESSAGE, Toast.LENGTH_SHORT).show();
                }
            } else if (preferences.getString(USERNAME_KEY, USERNAME_DEFAULT).equals(USERNAME_DEFAULT)) {
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString(USERNAME_KEY, userName);
                editor.putString(PASSWORD_KEY, password);
                editor.commit();
                login.setText(LOGIN);
                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                builder.setTitle(ALERT_TITLE);
                builder.setMessage(ALERT_MESSAGE);
                builder.setCancelable(true);
                builder.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                user.setText("");
                                pass.setText("");
                                dialog.cancel();
                            }
                        });
                builder.setNegativeButton("close",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            } else {
                Toast.makeText(LoginActivity.this, INVALID_USERNAME_MESSAGE, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
