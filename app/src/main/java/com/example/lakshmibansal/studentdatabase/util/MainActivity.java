package com.example.lakshmibansal.studentdatabase.util;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lakshmibansal.studentdatabase.R;
import com.example.lakshmibansal.studentdatabase.appConstants.DBController;
import com.example.lakshmibansal.studentdatabase.entities.Student;

import static com.example.lakshmibansal.studentdatabase.appConstants.AppConstants.*;

import com.example.lakshmibansal.studentdatabase.fragments.SpinnerFragment;
import com.example.lakshmibansal.studentdatabase.util.StudentActivity;

import java.util.Collections;
import java.util.Comparator;


public class MainActivity extends FragmentActivity {

    ListAdapter adapter;
    ListView listview;
    GridView gridview;
    Student studentObject;
    Button listBtn;
    Button gridBtn;
    int objectPosition;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listview = (ListView) findViewById(R.id.listView);
        gridview = (GridView) findViewById(R.id.gridView);
        listBtn = (Button) findViewById(R.id.list_option);
        gridBtn = (Button) findViewById(R.id.grid_option);
        SpinnerFragment spinnerFragment = new SpinnerFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.container,spinnerFragment).commit();



        // Set adapters for ListView and GridView
        adapter = new ListAdapter(MainActivity.this);
        listview.setAdapter(adapter);
        gridview.setAdapter(adapter);

        // Call to AsyncTask to update the List View
        new DBinteraction().execute(null, VIEW_ALL_KEY);

        // Setting Item Click Listeners for ListView
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                click(parent, view, position, id);
            }
        });

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                click(parent, view, position, id);
            }
        });

    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.createBtn:
                openAddStudent();
                break;
            case R.id.list_option:
                listBtn.setBackgroundColor(Color.parseColor(SELECTED_COLOR));
                gridBtn.setBackgroundColor(Color.parseColor(UNSELECTED_COLOR));
                listBtn.setTextColor(Color.parseColor(BLACK));
                gridBtn.setTextColor(Color.parseColor(WHITE));
                listview.setVisibility(View.VISIBLE);
                gridview.setVisibility(View.INVISIBLE);
                break;
            case R.id.grid_option:
                gridBtn.setBackgroundColor(Color.parseColor(SELECTED_COLOR));
                listBtn.setBackgroundColor(Color.parseColor(UNSELECTED_COLOR));
                gridBtn.setTextColor(Color.parseColor(BLACK));
                listBtn.setTextColor(Color.parseColor(WHITE));
                gridview.setVisibility(View.VISIBLE);
                listview.setVisibility(View.INVISIBLE);
                break;
            default:
                break;
        }
    }

    public void openAddStudent() {
        Intent intent = new Intent(this, StudentActivity.class);
        startActivityForResult(intent, ADD_REQUEST);
    }

    public void notifyMe(){
        adapter.notifyDataSetChanged();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ADD_REQUEST) {
            if (resultCode == RESULT_OK) {
                Bundle bundle = data.getExtras();
                adapter.student.add((Student) bundle.get(STUDENT_KEY));
                adapter.notifyDataSetChanged();
            } else if (resultCode == RESULT_CANCELED) {
            }
        }
        if (requestCode == EDIT_REQUEST) {
            if (resultCode == RESULT_OK) {
                Bundle bundle = data.getExtras();
                adapter.student.set(bundle.getInt(POSITION_KEY),(Student) bundle.get(STUDENT_KEY));
            } else if (resultCode == RESULT_CANCELED) {
            }
        }
        //dropdown.setSelection(0);
    }

    public void click(AdapterView<?> parent, View view, final int position, long id) {
        final Dialog options = new Dialog(MainActivity.this);
        options.setContentView(R.layout.item_dialog);
        options.setTitle(DIALOG_TITLE);
        Button viewBtn = (Button) options.findViewById(R.id.viewBtn);
        Button editBtn = (Button) options.findViewById(R.id.editBtn);
        Button deleteBtn = (Button) options.findViewById(R.id.deleteBtn);
        viewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DBinteraction().execute(adapter.getItem(position), VIEW_KEY);
                options.dismiss();
            }
        });
        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                objectPosition = position;
                new DBinteraction().execute(adapter.getItem(position), UPDATE_KEY);
                options.dismiss();
            }
        });
        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DBinteraction().execute(adapter.student.get(position), DELETE_KEY);
                adapter.student.remove(position);
                adapter.notifyDataSetChanged();
                options.dismiss();
            }
        });
        options.show();
    }

    public void sortByName() {
        Collections.sort(adapter.student, new Comparator<Student>() {
            @Override
            public int compare(Student lhs, Student rhs) {
                return lhs.getName().compareTo(rhs.getName());
            }
        });
        adapter.notifyDataSetChanged();
    }

    class DBinteraction extends AsyncTask<Object, Integer, Integer> {

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setTitle(DIALOG_TITLE);
            progressDialog.setMessage(DIALOG_MESSAGE);
            progressDialog.setIndeterminate(false);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(Object... params) {
            for (int i = INITIAL_VALUE; i < FINAL_VALUE; i++) {
                try {
                    Thread.sleep(SLEEP_TIME);
                    onProgressUpdate(i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            DBController dbController = new DBController(MainActivity.this);
            // For Extracting all values from database
            if (params[OPERATION_KEY].equals(VIEW_ALL_KEY)) {
                dbController.open();
                adapter.student = dbController.viewAllStudent();
                dbController.close();
                if (adapter.student.size() > 0) {
                    return EXTRACTION_SUCCESS;
                }
            } else if (params[OPERATION_KEY].equals(DELETE_KEY)) {
                dbController.open();
                int result = dbController.delete((Student) params[STUDENT_KEY_INDEX]);
                dbController.close();
                if (result > 0) {
                    return DELETE_SUCCESS;
                }
            } else if (params[OPERATION_KEY].equals(UPDATE_KEY)) {
                studentObject = (Student) params[STUDENT_KEY_INDEX];
                int rollNumber = studentObject.getRollno();
                dbController.open();
                studentObject = dbController.viewSingleStudents(rollNumber);
                dbController.close();
                if (studentObject.getRollno() > 0) {
                    return UPDATE_SUCCESS;
                }
            } else if (params[OPERATION_KEY].equals(VIEW_KEY)) {
                dbController.open();
                studentObject = (Student) params[STUDENT_KEY_INDEX];
                int rollNumber = studentObject.getRollno();
                studentObject = dbController.viewSingleStudents(rollNumber);
                dbController.close();
                if (studentObject.getRollno() > 0) {
                    return VIEW_SUCCESS;
                }
            }
            return FAILED_OPERATION;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            progressDialog.dismiss();

            // If the operation performed is failed
            if (integer == FAILED_OPERATION) {
                Toast.makeText(MainActivity.this,
                        NO_ENTRY_FOUND_MESSAGE, Toast.LENGTH_SHORT).show();
            }

            // For Extraction is successful
            else if (integer == EXTRACTION_SUCCESS) {
                adapter.notifyDataSetChanged();
                sortByRoll();
              //  dropdown.setSelection(0);
            }
            // For Deletion is successful
            else if (integer == DELETE_SUCCESS) {
                Toast.makeText(MainActivity.this,
                        DELETE_SUCCESS_MESSAGE, Toast.LENGTH_SHORT).show();
            }
            // For single Extraction is successful and open view activity(StudentOperations)
            else if (integer == VIEW_SUCCESS) {
                Intent intent = new Intent(MainActivity.this, StudentActivity.class);
                intent.putExtra(BUTTON_KEY, VIEW_BUTTON);
                intent.putExtra(STUDENT_KEY, studentObject);
                startActivity(intent);
            }
            // For single Extraction is successful and open edit activity(StudentOperations)
            else if (integer == UPDATE_SUCCESS) {
                Intent intent = new Intent(MainActivity.this, StudentActivity.class);
                intent.putExtra(BUTTON_KEY, EDIT_BUTTON);
                intent.putExtra(POSITION_KEY, objectPosition);
                intent.putExtra(STUDENT_KEY, studentObject);
                startActivityForResult(intent, EDIT_REQUEST);
            }
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            progressDialog.setProgress(values[0]);
        }
    }

    public void sortByRoll() {
        Collections.sort(adapter.student, new Comparator<Student>() {
            @Override
            public int compare(Student lhs, Student rhs) {
                return lhs.getRollno() - (rhs.getRollno());
            }
        });
    }
}