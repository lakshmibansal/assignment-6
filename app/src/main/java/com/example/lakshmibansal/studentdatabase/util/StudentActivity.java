package com.example.lakshmibansal.studentdatabase.util;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lakshmibansal.studentdatabase.appConstants.AppConstants;
import com.example.lakshmibansal.studentdatabase.appConstants.DBController;
import com.example.lakshmibansal.studentdatabase.R;
import com.example.lakshmibansal.studentdatabase.entities.Student;

import java.util.regex.Pattern;

import static com.example.lakshmibansal.studentdatabase.appConstants.AppConstants.*;

public class StudentActivity extends Activity implements AppConstants{
    EditText nameText;
    EditText rollText;
    EditText emailID, department, address;
    Button saveBtn, cancelBtn;
    Bundle extras;
    Student obj;
    static Student newStudent;
    TextView heading;
    int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student);
        nameText = (EditText) findViewById(R.id.name_box);
        rollText = (EditText) findViewById(R.id.roll_box);
        saveBtn = (Button) findViewById(R.id.saveBtn);
        cancelBtn = (Button) findViewById(R.id.cancelBtn);
        heading = (TextView) findViewById(R.id.heading);
        emailID = (EditText) findViewById(R.id.email_box);
        department = (EditText) findViewById(R.id.dept_box);
        address = (EditText) findViewById(R.id.address_box);
        extras = getIntent().getExtras();
        if (extras != null) {
            obj = (Student) extras.get(STUDENT_KEY);
            nameText.setText(obj.getName());
            rollText.setText(NULL_STRING + obj.getRollno());
            emailID.setText(obj.getEmail());
            department.setText(obj.getDepartment());
            address.setText(obj.getAddress());

            if (extras.getString(BUTTON_KEY).equals(VIEW_BUTTON)) {
                nameText.setEnabled(false);
                rollText.setEnabled(false);
                emailID.setEnabled(false);
                department.setEnabled(false);
                address.setEnabled(false);
                saveBtn.setVisibility(View.INVISIBLE);
                heading.setText(VIEW_ACTIVITY_TITLE);
                cancelBtn.setText(CLOSE_BUTTON_TEXT);
            }
            if (extras.getString(BUTTON_KEY).equals(EDIT_BUTTON)) {
                position = extras.getInt(POSITION_KEY);
                nameText.setText(obj.getName());
                rollText.setText("" + obj.getRollno());
                emailID.setText(obj.getEmail());
                department.setText(obj.getDepartment());
                address.setText(obj.getAddress());
                rollText.setEnabled(false);
                heading.setText(EDIT_ACTIVITY_TITLE);
                saveBtn.setText(UPDATE_TEXT);
            }
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.saveBtn:
                addStudent();
                break;
            case R.id.cancelBtn:
                Toast.makeText(this, CANCEL_MESSAGE, Toast.LENGTH_SHORT).show();
                finish();
            default:
                break;
        }
    }

    private void addStudent() {
        String name = nameText.getText().toString();
        String roll = rollText.getText().toString();
        String email = emailID.getText().toString();
        String dept = department.getText().toString();
        String add = address.getText().toString();

        if (name.equals("") || name.indexOf(" ") == 0) {
            Toast.makeText(this, INAPPROPRIATE_NAME_MESSAGE, Toast.LENGTH_SHORT).show();
        } else if (roll.equals("") || Integer.parseInt(roll) == 0) {
            Toast.makeText(this, INAPPROPRIATE_ROLL_MESSAGE, Toast.LENGTH_SHORT).show();
        } else if (!validEmail(email)) {
            Toast.makeText(this, INVALID_EMAIL_MESSAGE, Toast.LENGTH_SHORT).show();
        } else if (dept.equals("") || add.equals("")) {
            Toast.makeText(this, BLANK_FIELD_MESSAGE, Toast.LENGTH_SHORT).show();
        } else {
            // This operation will work if Edit button is clicked
            if (extras != null && extras.get(BUTTON_KEY).equals(EDIT_BUTTON)) {
                name = name.trim();
                newStudent = new Student(name, Integer.parseInt(roll), email, add, dept);
                new DBInteractions().execute(newStudent, UPDATE_KEY);

                // This operation will work if View button is clicked
            } else {
                name = name.trim();
                newStudent = new Student(name, Integer.parseInt(roll), email, add, dept);
                new DBInteractions().execute(newStudent, INSERT_KEY);
            }
        }
    }

    // This will validate the correct email pattern
    private boolean validEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }


    // This class will help in database interactions in background
    class DBInteractions extends AsyncTask<Object, Integer, Integer> {
        //ProgressDialog progressDialog;
        Intent intent = new Intent();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        // This is the actual method that will perform database actions in background
        protected Integer doInBackground(Object[] params) {
            for (int i = INITIAL_VALUE; i < FINAL_VALUE; i++) {
                try {
                    Thread.sleep(SLEEP_TIME);
                    onProgressUpdate(i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            DBController dbController = new DBController(StudentActivity.this);

            // For inserting a value in the database
            if (params[OPERATION_KEY].equals(INSERT_KEY)) {
                dbController.open();
                int result = (int) dbController.insert((Student) params[STUDENT_KEY_INDEX]);
                dbController.close();
                if (result > 0) {
                    return ADDED_SUCCESS;
                }

                // For updating a value in the database
            } else if (params[OPERATION_KEY].equals(UPDATE_KEY)) {
                dbController.open();
                int result = dbController.update((Student) params[STUDENT_KEY_INDEX]);
                dbController.close();
                if (result > 0) {
                    return UPDATE_SUCCESS;
                }
            }
            return FAILED_OPERATION;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
        }

        @Override
        // This method checks for results of the background action
        protected void onPostExecute(Integer o) {
            // If the operation performed is failed
            if (o == FAILED_OPERATION) {
                Toast.makeText(StudentActivity.this,
                        FAILED_OPERATION_MESSAGE, Toast.LENGTH_SHORT).show();
            }

            // For Insertion is successful
            else if (o == ADDED_SUCCESS) {
                intent.putExtra(STUDENT_KEY, newStudent);
                setResult(RESULT_OK, intent);
                Toast.makeText(StudentActivity.this,
                        ADDED_SUCCESS_MESSAGE, Toast.LENGTH_SHORT).show();
                finish();
            }

            // For Update is successful
            else if (o == UPDATE_SUCCESS) {
                intent.putExtra(STUDENT_KEY, newStudent);
                intent.putExtra(POSITION_KEY, position);
                setResult(RESULT_OK, intent);
                Toast.makeText(StudentActivity.this,
                        UPDATE_SUCCESS_MESSAGE, Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }
}

