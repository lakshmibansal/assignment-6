package com.example.lakshmibansal.studentdatabase.util;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.lakshmibansal.studentdatabase.fragments.GridFragment;
import com.example.lakshmibansal.studentdatabase.fragments.ListFragmentView;

import  static com.example.lakshmibansal.studentdatabase.appConstants.AppConstants.*;

/**
 * Created by LAKSHMI BANSAL on 23/02/2015.
 */
public class pagerAdapter extends FragmentPagerAdapter{
    Bundle bundle;
    public pagerAdapter(FragmentManager fm ,Bundle bundle){
        super(fm);this.bundle = bundle;
    }
    @Override
    public Fragment getItem(int position) {
        switch(position) {

            case LIST:
                ListFragmentView listFragmentView = new ListFragmentView();
                listFragmentView.setArguments(bundle);
                return listFragmentView;
            case GRID:
                GridFragment gridFragment = new GridFragment();
                gridFragment.setArguments(bundle);
                return gridFragment;

            default: ListFragmentView listFragmentView1 = new ListFragmentView();
                listFragmentView1.setArguments(bundle);
                return listFragmentView1;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if(position == LIST){
            return "LIST";
        }else if(position  == GRID){
            return "GRID";
        }
        return null;
    }
}
